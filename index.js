const express = require('express');
const mongoose = require('mongoose');
const userSchema = require('./models/user');
const recipeSchema = require('./models/recipe');

const app = express();

//middleware
app.use(express.json());
//mongoDB
mongoose.connect('mongodb://poli:password@localhost:27017/recipes?authSource=admin')
.then( () => console.log("conectado a mongo"))
.catch((error) => console.log(error))

app.get('/', (req, res) => {
    res.send('hi world');
})

app.post('/new_user', (req, res) => {
    const user = userSchema(req.body)
    user.save()
    .then((data) => res.json(data))
    .catch((error) => res.send(error));
})

app.post('/new_recipe', (req, res ) => {
    const recipe = recipeSchema(req.body)
    recipe.save()
    .then((data) => res.json(data))
    .catch((error) => res.send(error));
})

app.post('/rate' , (req, res) => {
    const {recipeId, userId, rating} = req.body
    recipeSchema.updateOne(
        { _id: recipeId},
        [
            { $set: {ratings: {$concatArrays: [{$ifNull: ['$ratings', []]},[{user: userId, rating: rating}]]}} },
            {$set: {avgRating: {$trunc: [{$avg: ['$ratings.rating']}, 0]}}}
        ]

    )
    .then((data) => res.json(data))
    .catch((error) => {
        console.log(error)
        res.send(error);

    res.send(req.body);
    });
});
app.get('/recipes', (req, res) => {
    const {userId, recipeId} = req.body
    if (recipeId){
        recipeSchema
            .find( {_id: recipeId})
            .then((data) => res.json(data))
            .catch((error) => res.json({message: error}))
    }else{
        recipeSchema
        .find({userId: userId})
        .then((data) => res.json(data))
        .catch((error) => res.json({message: error}))
    }
})


app.listen(3000, () => console.log ("escuchando en el puerto 3000..."))