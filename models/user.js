const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    Schema: {
        type: Number,
        required: true,
    },
    name: {
        type: String,
        required: true,
        lowercase: true,
    }
});

module.exports = mongoose.model('user', userSchema);